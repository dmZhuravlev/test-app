export class Card {
    name: String;
    title: String;
    description: String;
    isNotAble: boolean;
    path: string;
}