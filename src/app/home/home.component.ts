import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { Card } from '../_models';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less']
})

export class HomeComponent implements OnInit {
  	cardsList: Card[] = [
		{ 
			name: 'social-net',
			title: 'Social Networks',
			description: 'Manage your social activites easly with our Social Networks client',
			path: 'social-networks',
			isNotAble: false
		},
		{ 
			name: 'marketplace-net',
			title: 'Marketplace Networks',
			description: 'Manage your social activites easly with our Social Networks client',
			path: 'marketplace-network',
			isNotAble: false
		},
		{ 
			name: 'crm',
			title: 'CRM',
			description: 'Manage your social activites easly with our Social Networks client',
			path: '',
			isNotAble: true,
		},
		{ 
			name: 'web-analytics',
			title: 'Web Analytics',
			description: 'Explore your bisness due to our proffesional tools for marketers',
			path: 'web-analytics',
			isNotAble: false,
		},
		{ 
			name: 'reports',
			title: 'Reports / Charts',
			description: 'Explore your bisness due to our proffesional tools for marketers',
			path: '',
			isNotAble: true,
		},
		{ 
			name: 'landing',
			title: 'Landing Page',
			description: 'Explore your bisness due to our proffesional tools for marketers',
			path: '',
			isNotAble: true,
		},
		{ 
			name: 'mail',
			title: 'Email Client',
			description: 'Resolve all your mail tasks in our email fast and simple client',
			path: 'email-client',
			isNotAble: false,
		},
	];

	constructor(private router: Router,) {}

	ngOnInit() {}

	cardClick(activeTab: string) {
		this.router.navigate(['/' + activeTab]);
	}
}
