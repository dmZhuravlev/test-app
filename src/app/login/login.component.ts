import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../_services';

@Component({
    templateUrl: 'login.component.html',
    styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {
    error: string;
    activeTab: boolean;

    constructor(
        private router: Router,
        private authenticationService: AuthenticationService,
    ) {
        // redirect to home if already logged in
        if (this.authenticationService.currentUserValue) { 
            this.router.navigate(['/']);
        }
    }

    ngOnInit() {
        this.activeTab = this.router.url.indexOf('/sign-in') !== -1;
    }

    goTo(path:string) {
        if (path === this.router.url) {
            return;
        }
        this.router.navigate([path]);
    }
}
