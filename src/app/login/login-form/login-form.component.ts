import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AlertService, AuthenticationService } from '../../_services';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  	selector: 'app-login-form',
  	templateUrl: './login-form.component.html',
	styleUrls: [
		'./../login.component.less',
		'./../../_components/button/button.component.less'
	]
})

export class LoginFormComponent implements OnInit {
	loginForm: FormGroup;
	submitted = false;
	loading = false;
	error: string;
	returnUrl: string;

	constructor(
		private formBuilder: FormBuilder,
		private authenticationService: AuthenticationService,
		private alertService: AlertService,
		private route: ActivatedRoute,
		private router: Router,
	) { }

	ngOnInit() {
		this.loginForm = this.formBuilder.group({
			email: ['', Validators.required],
			password: ['', Validators.required],
		});

		// get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
	}

	// convenience getter for easy access to form fields
	get f() {
		return this.loginForm.controls;
	}

	onSubmitLogin() {
        this.submitted = true;
        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }

        this.loading = true;
        this.authenticationService.login(this.f.email.value, this.f.password.value)
            .pipe(first())
            .subscribe(
                data => {
                    this.router.navigate([this.returnUrl]);
                },
                error => {
                    this.error = error.error.message;
                    this.alertService.error(error);

                    this.loading = false;
                });
    }
}
