import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertService, AuthenticationService } from '../../_services';
import { first } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  	selector: 'app-register-form',
  	templateUrl: './register-form.component.html',
  	styleUrls: [
		'./../login.component.less',
		'./../../_components/button/button.component.less'
	]
})
export class RegisterFormComponent implements OnInit {
	registerForm: FormGroup;
	submittedRegister = false;
	loading = false;
	error: string;
	returnUrl: string;

  	constructor(
		private formBuilder: FormBuilder,
		private authenticationService: AuthenticationService,
		private alertService: AlertService,
		private route: ActivatedRoute,
		private router: Router,
	  ) { }

  	ngOnInit() {
		this.registerForm = this.formBuilder.group({
			username: ['', Validators.required],
			email: ['', Validators.required],
			password: ['', Validators.required],
			confirmPassword: ['', Validators.required], 
		});

		// get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  	}

	// convenience getter for easy access to form fields
	get form() {
		return this.registerForm.controls;
	}

	onSubmitRegister() {
        this.submittedRegister = true;
        // stop here if form is invalid
        if (this.registerForm.invalid) {
            return;
        }

        this.loading = true;
        this.authenticationService.register(this.form.username.value,
            this.form.email.value,this.form.password.value, this.form.confirmPassword.value)
            .pipe(first())
            .subscribe(
                data => {
                    this.router.navigate([this.returnUrl]);
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }

}
