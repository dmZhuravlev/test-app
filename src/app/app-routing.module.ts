import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login';
import { HomeComponent } from './home';
import { SocialNetworksComponent } from './social-networks';
import { MarketplaceNetworkComponent } from './marketplace-network';
import { WebAnalyticsComponent } from './web-analytics';
import { EmailClientComponent } from './email-client';
import { AuthGuard } from './_guards';

const routes: Routes = [
  {
    path: 'sign-in',
    component: LoginComponent
  },
  {
    path: 'sign-up',
    component: LoginComponent
  },
  {
    path: 'social-networks',
    component: SocialNetworksComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'marketplace-network',
    component: MarketplaceNetworkComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'web-analytics',
    component: WebAnalyticsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'email-client',
    component: EmailClientComponent,
    canActivate: [AuthGuard]
  },
  {
    path: '',
    component: HomeComponent,
    canActivate: [AuthGuard]
  },
  {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }