import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
//import { enableProdMode } from '@angular/core';

// Modules
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ClarityModule } from '@clr/angular';

// used to create fake backend
import { fakeBackendProvider } from './_helpers';

import { AppRoutingModule } from './app-routing.module';

import { MainComponent } from './main';
import { LoginComponent } from './login';
import { HomeComponent } from './home';
import { AlertComponent } from './_components/alert';
import { LoginFormComponent } from './login/login-form';
import { RegisterFormComponent } from './login/register-form';
import { SocialNetworksComponent } from './social-networks';
import { MarketplaceNetworkComponent } from './marketplace-network';
import { WebAnalyticsComponent } from './web-analytics';
import { EmailClientComponent } from './email-client';

@NgModule({
	declarations: [
    	MainComponent,
		LoginComponent,
    	HomeComponent,
		AlertComponent,
		LoginFormComponent,
		RegisterFormComponent,
		SocialNetworksComponent,
		MarketplaceNetworkComponent,
		WebAnalyticsComponent,
		EmailClientComponent,
	],
	imports: [
		BrowserModule,
		RouterModule,
		AppRoutingModule,
		FormsModule,
		ReactiveFormsModule,
		BrowserAnimationsModule,
		HttpClientModule,
		NoopAnimationsModule,
		ClarityModule,
	],
  providers: [
	  // provider used to create fake backend
	  fakeBackendProvider
  ],
  bootstrap: [MainComponent]
})

// enableProdMode();

export class AppModule { }