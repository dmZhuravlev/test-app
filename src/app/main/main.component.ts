import { Component, OnInit } from '@angular/core';
import { Router, NavigationStart } from "@angular/router";
import { AuthenticationService } from '../_services';
import { filter } from 'rxjs/operators';

@Component({
    selector: 'app-root',
    templateUrl: './main.component.html',
    styleUrls: ['./main.component.less']
})
export class MainComponent implements OnInit {
    currentUser: object;
    activeNavTab: string;

    constructor(
        private router: Router,
        private authenticationService: AuthenticationService
    ){
        this.authenticationService.currentUser
            .subscribe(user => this.currentUser = user);

        router.events.pipe(filter(event => event instanceof NavigationStart))
            .subscribe((event: NavigationStart) => {
                this.activeNavTab = event.url.replace(/\//g, '');
            });
    }

    ngOnInit() {
    }

    logout() {
        this.authenticationService.logout();
        this.router.navigate(['/sign-in']);
    }

    navTabClick(activeTab: string) {
        this.activeNavTab = activeTab;
        this.router.navigate(['/' + activeTab]);
    }
}
