import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-social-networks',
	templateUrl: './social-networks.component.html',
	styleUrls: [
		'./social-networks.component.less',
		'../_components/button/button.component.less'
	]
})

export class SocialNetworksComponent implements OnInit {
	activeNavTab: string;

	constructor() { }

	ngOnInit() {
		this.activeNavTab = 'profiles';
	}

	navTabClick(activeTab: string) {
        this.activeNavTab = activeTab;
    }
}
